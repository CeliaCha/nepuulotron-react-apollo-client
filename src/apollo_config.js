import { ApolloClient } from "apollo-client"
import { onError } from "apollo-link-error"
import { createHttpLink } from "apollo-link-http"
import { InMemoryCache } from "apollo-cache-inmemory"
import { setContext } from "apollo-link-context"
import { split } from "apollo-link"
import { WebSocketLink } from "apollo-link-ws"
import { getMainDefinition } from "apollo-utilities"
import { AUTH_TOKEN, USER_NAME } from "./constants"

const httpLink = createHttpLink({
  uri: "https://peaceful-refuge-46904.herokuapp.com/"
})

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN)
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  }
})

const wsLink = new WebSocketLink({
  uri: `ws://peaceful-refuge-46904.herokuapp.com/`,
  options: {
    reconnect: true,
    connectionParams: {
      authToken: localStorage.getItem(AUTH_TOKEN)
    }
  }
})

const errorLink = onError(({ graphQLErrors }) => {
  if (graphQLErrors) graphQLErrors.map(({ message }) => console.log(message))
})

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === "OperationDefinition" && operation === "subscription"
  },
  wsLink,
  authLink.concat(httpLink),
  errorLink
)
const cache = new InMemoryCache()

const client = new ApolloClient({
  cache,
  link
})

const userName = localStorage.getItem(USER_NAME)
cache.writeData({ data: { userName } })

export default client
