import React from "react"
import { Route, BrowserRouter as Router } from "react-router-dom"
import Header from "./Header"
import Home from "./Home"
import Login from "./Login"
import CreateArticle from "./CreateArticle"
import "bootstrap/dist/css/bootstrap.min.css"

// import "styles/App.css"

function App() {
  return (
    <div className="container">
      <Router>
        <Header />
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/addarticle" component={CreateArticle} />
      </Router>
    </div>
  )
}

export default App
