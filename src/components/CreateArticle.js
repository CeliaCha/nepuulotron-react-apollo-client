/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from "react"
import { useMutation } from "@apollo/react-hooks"
import gql from "graphql-tag"

import Section from "./Section"

const POST_ARTICLE_MUTATION = gql`
  mutation PostArticleMutation($category: String!, $title: String!, $image: String, $sections: [InputCreateSection!]) {
    postArticle(category: $category, title: $title, image: $image, sections: $sections) {
      id
      category
      title
    }
  }
`

const CreateArticle = () => {
  const [article, setArticle] = useState()
  const [title, setTitle] = useState()
  const [image, setImage] = useState()
  const [category, setCategory] = useState()
  const [text, setText] = useState()
  const [addArticle, { data }] = useMutation(POST_ARTICLE_MUTATION)
  const handleSubmit = e => {
    e.preventDefault()
    const sections = text
      .split(/\r\n|\n\r|\n|\r/g) // divide by paragraphs
      .filter(item => item.match(/\S/)) // remove empty paragraphs
      .reduce(
        (acc, cv, index) => {
          const isFirstVal = !acc[acc.length - 1].title
          const isTitle = cv.trim().length < 35
          const isTextWithTitle = acc[acc.length - 1].title
          if (isFirstVal) {
            return isTitle ? [{ title: cv.trim(), text: "" }] : [{ title: null, text: cv.trim() }]
          }
          if (isTitle) {
            return [...acc, { title: cv.trim(), text: "" }]
          }
          if (isTextWithTitle) {
            const newtext = `${acc[acc.length - 1].text}\n${cv.trim()}`
            acc[acc.length - 1].text = newtext
            return acc
          }
          return [...acc, { title: null, text: cv.trim() }]
        },
        [{ title: null, text: null }]
      )
    // // setArticle({ category, title, image, sections })
    console.log(sections)
    addArticle({ variables: { category, title, image, sections } })
  }
  return (
    <>
      <h1>Create Article</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Titre : <input type="text" onBlur={e => setTitle(e.target.value)} />
        </label>
        <label>
          Image (url) : <input type="text" onBlur={e => setImage(e.target.value)} />
        </label>
        <label>
          Catégorie :
          <select onBlur={e => setCategory(e.target.value)}>
            <option value="general">Général</option>
            <option value="combat">Combat</option>
            <option value="characters">Personnages</option>
            <option value="gear">Equipement</option>
            <option value="travel">Voyages</option>
            <option value="corruption">Corruption</option>
            <option value="audiences">Audiences</option>
            <option value="fellowship">Communauté</option>
          </select>
        </label>
        <label>
          Contenu :
          <textarea cols="80" rows="5" type="text" onBlur={e => setText(e.target.value)} />
        </label>
        <input type="submit" value="Prévisualiser" />
      </form>
      {/* Display result */}
      {article && (
        <>
          <p className="category">{article.category}</p>
          <p className="title">{article.title}</p>
          <img src={article.image} alt={article.title} height="170" width="390" />
          <div className="text">
            {article.sections.map(item => (
              <Section data={item} key={item.index} />
            ))}
          </div>
        </>
      )}
    </>
  )
}

export default CreateArticle
