import React, { useState } from "react"
import { Link as RouterLink } from "react-router-dom"
import gql from "graphql-tag"
import { useQuery } from "@apollo/react-hooks"

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from "reactstrap"

const GET_USER_NAME = gql`
  {
    userName @client
  }
`

function Header() {
  const {
    data: { userName }
  } = useQuery(GET_USER_NAME)

  return (
    <header>
      <Navbar light expand="md">
        <NavbarBrand tag={RouterLink} to="/">
          NEBALOTRON
        </NavbarBrand>
        <Nav className="mr-auto" navbar>
          {/* <NavItem>
            <NavLink tag={RouterLink} to="/">
              Home
            </NavLink>
          </NavItem> */}
          <NavItem>
            <NavLink tag={RouterLink} to="/login">
              Login
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={RouterLink} to="/addarticle">
              Ajouter article
            </NavLink>
          </NavItem>
        </Nav>
        <NavbarText>{userName?.toUpperCase() || "Utilisateur non connecté"}</NavbarText>
      </Navbar>
    </header>
  )
}

export default Header
