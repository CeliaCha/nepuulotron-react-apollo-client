import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { Spinner } from "reactstrap"
import { FEED_QUERY } from "../operations"
import { AUTH_TOKEN } from "../constants"

function Home({ history }) {
  const auth = localStorage.getItem(AUTH_TOKEN)
  if (!auth) history.push("/login")
  const { data, loading } = useQuery(FEED_QUERY)
  console.log(data)
  return (
    <div>
      {loading && <Spinner color="dark" />}
      {data && data.feed.articles.map(article => <p>{article.title}</p>)}
    </div>
  )
}

export default Home
