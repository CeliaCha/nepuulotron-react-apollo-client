/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react"
import { useMutation, useApolloClient } from "@apollo/react-hooks"
import { AUTH_TOKEN, USER_NAME } from "../constants"
import { SIGNUP_MUTATION, LOGIN_MUTATION } from "../operations"

const Login = ({ history }) => {
  const client = useApolloClient()
  const [login, setLogin] = useState(true)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [name, setName] = useState("")
  const authMutation = login ? LOGIN_MUTATION : SIGNUP_MUTATION
  const [submit, { loading, error }] = useMutation(authMutation)

  const _confirm = async () => {
    const variables = { name, email, password }
    submit({ variables }).then(payload => {
      const { token, user } = login ? payload.data.login : payload.data.signup
      console.log(payload)
      client.writeData({ data: { userName: user.name } })
      localStorage.setItem(AUTH_TOKEN, token)
      localStorage.setItem(USER_NAME, user.name)
      history.push(`/`)
    })
  }

  return (
    <div>
      {loading && <h1>Loading...</h1>}
      <h4 className="mv3">{login ? "Login" : "Sign Up"}</h4>
      <div className="form-group">
        {!login && <input value={name} onChange={e => setName(e.target.value)} type="text" placeholder="Your name" />}
        <input
          className="form-input"
          value={email}
          onChange={e => setEmail(e.target.value)}
          type="text"
          placeholder="Your email address"
        />
        <input
          value={password}
          onChange={e => setPassword(e.target.value)}
          type="password"
          placeholder="Choose a safe password"
        />
      </div>
      <div>
        <button type="button" onClick={_confirm}>
          {login ? "login" : "create account"}
        </button>
        <div onClick={() => setLogin(!login)}>{login ? "need to create an account?" : "already have an account?"}</div>
      </div>
    </div>
  )
}

export default Login
