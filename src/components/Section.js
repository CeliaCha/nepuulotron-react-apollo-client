import React from "react"

const Section = ({ data }) => {
  return (
    <div>
      <p className="subtitle">{data.title}</p>
      {data.content.split("\n").map(el => (
        <p className="text" key={el}>
          {el}
        </p>
      ))}
    </div>
  )
}

export default Section
