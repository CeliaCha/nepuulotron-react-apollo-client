import gql from "graphql-tag"

const FEED_QUERY = gql`
  query FeedQuery($first: Int, $skip: Int, $orderBy: ArticleOrderByInput) {
    feed(first: $first, skip: $skip, orderBy: $orderBy) {
      articles {
        id
        title
        category
        sections {
          title
          text
        }
      }
    }
  }
`

const POST_ARTICLE_MUTATION = gql`
  mutation PostArticleMutation($category: String!, $title: String!, $image: String, $sections: [InputCreateSection!]) {
    postArticle(category: $category, title: $title, image: $image, sections: $sections) {
      id
      createdAt
      category
      title
      image
      sections {
        id
        title
        text
      }
    }
  }
`

const FEED_SEARCH_QUERY = gql`
  query FeedSearchQuery($filter: String!) {
    feed(filter: $filter) {
      links {
        id
        url
        description
        createdAt
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
    }
  }
`

export { FEED_QUERY, FEED_SEARCH_QUERY, POST_ARTICLE_MUTATION }
